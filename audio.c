#include "audio.h"

//Unpacks len nibbles from array ptr packing them into a u32
static inline uint32_t unpack(uint8_t *ptr, uint8_t len)
{
	uint32_t ret = 0;
	for (int i = 0; i < len; i++)
		ret |= ptr[i] << (i * 4);
	return ret;
}

//Repacks len nibbles from u32 to array ptr
static inline void repack(uint32_t val, uint8_t *ptr, uint8_t len)
{
	for (int i = 0; i < len; i++)
		ptr[i] = (val & (0xF << (i * 4))) >> (i * 4);
}

//Update accumulator with length len acc with frequency freq, return new value
static uint32_t update_acc(uint8_t *acc, uint8_t *freq, uint8_t len)
{
	uint32_t f = unpack(freq, len);
	uint32_t a = unpack(acc, len);

	//Update accumulator, mask bits
	a = (a + f) & (0xFFFFFFFF >> (32 - (4 * len)));

	//Write accumulator back to memory
	repack(a, acc, len);
	return a;
}

//Return signed sample from rom using waveform and accumulator multipled by vol
static int8_t calc_sample(uint8_t *rom, uint32_t acc, uint8_t wav, uint8_t vol, uint8_t len)
{
	//Calculate ROM address from wavform and accumulator
	uint16_t addr =
		(wav << 5) | //Waveform
		(acc >> ((len * 4) - 5)); //Waveform offset

	//Convert to signed, set volume
	return (rom[addr] - 8) * vol;
}

int16_t audio_cycle(audio_state *s)
{
	//Update voice accumulators
	uint32_t acc1 = update_acc(s->v1_acc, s->v1_freq, 5);
	uint32_t acc2 = update_acc(s->v2_acc, s->v2_freq, 4);
	uint32_t acc3 = update_acc(s->v3_acc, s->v3_freq, 4);

	//Calculate sample for each voice
	int8_t v1 = calc_sample(s->ROM, acc1, s->v1_wav, s->v1_vol, 5);
	int8_t v2 = calc_sample(s->ROM, acc2, s->v2_wav, s->v2_vol, 4);
	int8_t v3 = calc_sample(s->ROM, acc3, s->v3_wav, s->v3_vol, 4);

	//Mix and scale
	int16_t ret = (v1 + v2 + v3) * 90;
	return (s->en) ? ret : 0;
}
