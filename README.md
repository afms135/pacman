# Pac-Man Emulator

![Screenshot](screenshot.png)

This project is an emulator for the original Pac-Man arcade cabinet.
Requires SDL2.

Requires an unextracted `pacman.zip` ROM set with the following files:
- `pacman.6e`
- `pacman.6f`
- `pacman.6h`
- `pacman.6j`
- `pacman.5e`
- `pacman.5f`
- `82s126.4a`
- `82s123.7f`
- `82s126.1m`
- `82s126.3m`

## Controls

|     Key     |       Control      |
|-------------|--------------------|
| c           | Add credit         |
| Return / 1  | Start game (1P)    |
| 2           | Start game (2P)    |
| Up arrow    | Move up (P1)       |
| Down arrow  | Move down (P1)     |
| Left arrow  | Move left (P1)     |
| Right arrow | Move right (P1)    |
| w           | Move up (P2)       |
| s           | Move down (P2)     |
| a           | Move left (P2)     |
| d           | Move right (P2)    |
| m           | Toggle mute        |
| r           | Rack advance       |
| t           | Tilt               |

## DIP Switches

The arcade machine configuration can be set the top of `pacman.c`:

- `DIP10` determines how many credits are required to start the game
(0=freeplay, 1=1 credit, 2=2 credits, 3=1 credit every 2 coins).

- `DIP32` determines how many lives the player has per game
(0=1 life, 1=2 lives, 2=3 lives, 3=5 lives).

- `DIP54` determines how many points are required to gain a life
(0=10000 points, 1=15000 points, 2=20000 points, 3=none).

- `DIP6` determines the difficulty (0=Hard, 1=Normal).

- `DIP7` detemines which ghost names are used (0=alternate, 1=normal).

## Compile

To compile run:

`$ gcc z80.c audio.c video.c pacman.c -lSDL2`

To compile the Z80 CPU core tests run:

`$ gcc z80.c z80_test.c`
