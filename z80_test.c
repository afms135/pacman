#include "z80.h"
#include <stdio.h>

static int running = 1;
static uint8_t mem[65536] = { 0 };
static int col = 0; //Screen column
static int cmd = 0; //Screen command

uint8_t mem_rd(z80_state *s, uint16_t addr)
{
#define BKPT(address) ((s->PC - 1) == (address) && addr == (address))

	if (BKPT(0x0000)) //Final RET instruction
		running = 0;
	else if (BKPT(0x1601)) //Open channel (ZX ROM)
		return 0xC9; //RET
	else if (BKPT(0x0010)) //RST 10h (ZX Print A)
	{
		if (cmd)
		{
			if (cmd == 0x17)
			{
				int x = s->A - col;
				while (x-- > 0)
					putchar(' ');
			}
			cmd = 0;
		}
		else if (s->A == '\r') //Newline
		{
			putchar('\r');
			putchar('\n');
			col = 0;
		}
		else if (s->A >= 0x10 && s->A <= 0x17) //Command
			cmd = s->A;
		else if (s->A >= 0x20 && s->A <= 0x7E) //Printable ASCII
		{
			putchar(s->A);
			col++;
		}
		else if (s->A == 0x7F) //DEL = Copyright symbol
		{
			putchar('C');
			col++;
		}
		return 0xC9; //RET
	}
	return mem[addr];
#undef BKPT
}

void mem_wr(z80_state *s, uint16_t addr, uint8_t val)
{
	mem[addr] = val;
}

uint8_t io_rd(struct z80_state *state, uint8_t port)
{
	return 0xBF; //For z80test
}

void io_wr(struct z80_state *state, uint8_t port, uint8_t val)
{
}

int runtest(z80_state *s, char *file, int n)
{
	printf("Test %u: %s\n", n, file);
	printf("--------------------------------\n");

	FILE *f = fopen(file, "rb");
	if (f == NULL)
		return -1; //Could not open file
	fseek(f, 0x5b, SEEK_SET); //Start of code
	fread(mem + 0x8000, 1, sizeof(mem) - 0x8000, f);
	fclose(f);

	z80_reset(s);
	s->PC = 0x8000;
	running = 1;
	while (running)
		z80_cycle(s);
	printf("\n--------------------------------\n\n");
	return 0;
}

int main(void)
{
	z80_state s;
	s.m_rd = mem_rd;
	s.m_wr = mem_wr;
	s.io_rd = io_rd;
	s.io_wr = io_wr;

	/*
	 * Print opcode cycle tables
	 */
	printf("-----------------------------------------------\n");
	printf("Opcode cycle tables\n");
	printf("-----------------------------------------------\n");
	printf("Base opcodes\n");
	for (int i = 0; i < 256; i++)
	{
		//Skip prefix opcodes
		if (i == 0xCB || i == 0xDD || i == 0xED || i == 0xFD)
		{
			printf(" _ ");
			continue;
		}
		z80_reset(&s);
		s.PC = 0x100;
		mem[0x100] = i;
		printf("%2i ", z80_cycle(&s));
		if (((i + 1) % 16) == 0)
			printf("\n");
	}
	printf("-----------------------------------------------\n");
	printf("ED Prefix\n");
	for (int i = 0; i < 256; i++)
	{
		z80_reset(&s);
		s.PC = 0x100;
		//Ensure block instrutions only run once
		if (i == 0xB0 || i == 0xB1 || i == 0xB8 || i == 0xB9)
			s.BC = 0x0002;
		else if (i == 0xB2 || i == 0xB3 || i == 0xBA || i == 0xBB)
			s.B = 0x02;
		mem[0x100] = 0xED;
		mem[0x101] = i;
		printf("%2i ", z80_cycle(&s));
		if (((i + 1) % 16) == 0)
			printf("\n");
	}
	printf("-----------------------------------------------\n");
	printf("CB Prefix\n");
	for (int i = 0; i < 256; i++)
	{
		z80_reset(&s);
		s.PC = 0x100;
		mem[0x100] = 0xCB;
		mem[0x101] = i;
		printf("%2i ", z80_cycle(&s));
		if (((i + 1) % 16) == 0)
			printf("\n");
	}
	printf("-----------------------------------------------\n");
	printf("DD/FD Prefix\n");
	for (int i = 0; i < 256; i++)
	{
		if (i == 0xCB) //Skip prefix opcode
		{
			printf(" _ ");
			continue;
		}
		z80_reset(&s);
		s.PC = 0x100;
		mem[0x100] = 0xDD;
		mem[0x101] = i;
		int ret = z80_cycle(&s);
		printf("%2i ", ret + z80_cycle(&s));
		if (((i + 1) % 16) == 0)
			printf("\n");
	}
	printf("-----------------------------------------------\n");
	printf("DDCB/FDCB Prefix\n");
	for (int i = 0; i < 256; i++)
	{
		z80_reset(&s);
		s.PC = 0x100;
		mem[0x100] = 0xDD;
		mem[0x101] = 0xCB;
		mem[0x102] = 0x00;
		mem[0x103] = i;
		int ret = z80_cycle(&s);
		printf("%2i ", ret + z80_cycle(&s));
		if (((i + 1) % 16) == 0)
			printf("\n");
	}
	printf("-----------------------------------------------\n");

	char *tests[] =
	{
		"tests/z80docflags.tap", //Documented flags
		"tests/z80doc.tap",      //Documented flags and registers
		"tests/z80flags.tap",    //All flags
		"tests/z80full.tap",     //All flags and registers
		"tests/z80ccf.tap",      //Tests XF YF CCF behaviour after every instruction
		"tests/z80memptr.tap",   //Tests WZ(MEMPTR) behaviour after every instruction
	};

	for (size_t i = 0; i < (sizeof(tests) / sizeof(char*)); i++)
	{
		int ret = runtest(&s, tests[i], i);
		if (ret != 0)
			return ret; //Error occured, exit
	}
	return 0;
}
