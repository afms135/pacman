#ifndef VIDEO_H
#define VIDEO_H
#include <stdint.h>

typedef struct video_state
{
	//ROMS
	uint8_t ROM_tile[4096];
	uint8_t ROM_sprite[4096];
	uint8_t ROM_palette[256];
	uint8_t ROM_colour[32];

	//Tile VRAM
	uint8_t VRAM_tile[1024];
	uint8_t VRAM_pal[1024];

	//Sprite registers
	uint8_t sprite_attrib[16];
	uint8_t sprite_xy[16];

	//Output
	uint32_t screen[288][224];
} video_state;

void video_draw(video_state *s);

#endif /*VIDEO_H*/
