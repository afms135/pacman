#ifndef Z80_H
#define Z80_H
#include <stdint.h>

/*
 * Define a register pair
 */
#define REGDEF(R16, R8H, R8L) \
	union \
	{ \
		struct \
		{ \
			uint8_t R8L; \
			uint8_t R8H; \
		}; \
		uint16_t R16; \
	} \

/*
 * Compile time options
 */
//#define Z80_DEBUG //Enable z80_printreg() (requires printf())
//#define Z80_BIGENDIAN //Define on big endian systems

/*
 * Z80 state
 */
typedef struct z80_state
{
	//Registers
#ifdef Z80_BIGENDIAN
	REGDEF(AF, F, A);
	REGDEF(BC, C, B);
	REGDEF(DE, E, D);
	REGDEF(HL, L, H);
	REGDEF(IX, IXL, IXH);
	REGDEF(IY, IYL, IYH);
#else //Little endian
	REGDEF(AF, A, F);
	REGDEF(BC, B, C);
	REGDEF(DE, D, E);
	REGDEF(HL, H, L);
	REGDEF(IX, IXH, IXL);
	REGDEF(IY, IYH, IYL);
#endif
	uint16_t PC;
	uint16_t SP;
	uint16_t WZ;
	uint8_t I;
	uint8_t R;
	uint8_t Q;

	//Alternate registers
	uint16_t alt_AF;
	uint16_t alt_BC;
	uint16_t alt_DE;
	uint16_t alt_HL;

	//Interrupts
	uint8_t IFF1;
	uint8_t IFF2;
	uint8_t IM;

	//State flags
	uint8_t halted;
	uint8_t ixiy;
	uint8_t ei_delay;
	uint8_t int_pending;
	uint8_t int_val;
	uint8_t nmi_pending;

	//Read/Write callbacks
	uint8_t(*m_rd)(struct z80_state *state, uint16_t addr);
	void(*m_wr)(struct z80_state *state, uint16_t addr, uint8_t val);
	uint8_t(*io_rd)(struct z80_state *state, uint8_t port);
	void(*io_wr)(struct z80_state *state, uint8_t port, uint8_t val);
} z80_state;

/*
 * API
 */
//Reset Z80 CPU
void z80_reset(z80_state *s);

//Execute one CPU instruction
int z80_cycle(z80_state *s);

//Signal a maskable interrupt
void z80_int(z80_state *s, uint8_t v);

//Signal a non-maskable interrupt
void z80_nmi(z80_state *s);

//Print Z80 register states
#ifdef Z80_DEBUG
#include <stdio.h>
void z80_printreg(z80_state *s);
#endif

#endif /*Z80_H*/
