#include "z80.h"

/*
// Cycle count tables
*/
static int cycle_base[256] = {
	 4, 10,  7,  6,  4,  4,  7,  4,  4, 11,  7,  6,  4,  4,  7,  4,
	 8, 10,  7,  6,  4,  4,  7,  4, 12, 11,  7,  6,  4,  4,  7,  4,
	 7, 10, 16,  6,  4,  4,  7,  4,  7, 11, 16,  6,  4,  4,  7,  4,
	 7, 10, 13,  6, 11, 11, 10,  4,  7, 11, 13,  6,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 7,  7,  7,  7,  7,  7,  4,  7,  4,  4,  4,  4,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 4,  4,  4,  4,  4,  4,  7,  4,  4,  4,  4,  4,  4,  4,  7,  4,
	 5, 10, 10, 10, 10, 11,  7, 11,  5, 10, 10,  4, 10, 17,  7, 11,
	 5, 10, 10, 11, 10, 11,  7, 11,  5,  4, 10, 11, 10,  4,  7, 11,
	 5, 10, 10, 19, 10, 11,  7, 11,  5,  4, 10,  4, 10,  4,  7, 11,
	 5, 10, 10,  4, 10, 11,  7, 11,  5,  6, 10,  4, 10,  4,  7, 11
};

static int cycle_ED[256] = { //Prefix byte cycles contained in base table
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 8,  8, 11, 16,  4,  10, 4,  5,  8,  8, 11, 16,  4,  10, 4,  5,
	 8,  8, 11, 16,  4,  10, 4,  5,  8,  8, 11, 16,  4,  10, 4,  5,
	 8,  8, 11, 16,  4,  10, 4, 14,  8,  8, 11, 16,  4,  10, 4,  14,
	 8,  8, 11, 16,  4,  10, 4,  4,  8,  8, 11, 16,  4,  10, 4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	12, 12, 12, 12,  4,  4,  4,  4,  12, 12, 12, 12, 4,  4,  4,  4,
	12, 12, 12, 12,  4,  4,  4,  4,  12, 12, 12, 12, 4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,
	 4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4
};

static int cycle_CB[256] = { //Prefix byte cycles contained in base table
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4,  8,  4,  4,  4,  4,  4,  4,  4,  8,  4,
	 4,  4,  4,  4,  4,  4,  8,  4,  4,  4,  4,  4,  4,  4,  8,  4,
	 4,  4,  4,  4,  4,  4,  8,  4,  4,  4,  4,  4,  4,  4,  8,  4,
	 4,  4,  4,  4,  4,  4,  8,  4,  4,  4,  4,  4,  4,  4,  8,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4,
	 4,  4,  4,  4,  4,  4, 11,  4,  4,  4,  4,  4,  4,  4, 11,  4
};

static int cycle_DDFD[256] = { //Table contains increase in cycles from base not including prefix
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  8,  8,  5,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 8,  8,  8,  8,  8,  8,  0,  8,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  8,  0,  0,  0,  0,  0,  0,  0,  8,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  4,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
};

static int cycle_XYCB[256] = { //Prefix byte cycles contained in base and DDFD tables
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	 8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,
	 8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,
	 8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,
	 8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,  8,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11,
	11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11
};

/*
// Flag register bit definitions
*/
enum flags
{
	C  = 0,
	N  = 1,
	PV = 2,
	X  = 3,
	H  = 4,
	Y  = 5,
	Z  = 6,
	S  = 7
};
#define GET_FLAG(f) (!!(s->F & (1UL << (f))))
#define SET_FLAG(f, x) (s->F = (s->F & ~(1UL << (f))) | (!!(x) << (f)))

/*
// Helper functions
*/
//Read 16-bit value from memory
static uint16_t m_rd16(z80_state *s, uint16_t addr)
{
	uint8_t low = s->m_rd(s, addr);
	uint8_t high = s->m_rd(s, addr + 1);
	return (high << 8) | low;
}

//Write 16-bit value to memory
static void m_wr16(z80_state *s, uint16_t addr, uint16_t v)
{
	s->m_wr(s, addr, v & 0xFF); //Low
	s->m_wr(s, addr + 1, v >> 8); //High
}

//Returns an instruction 8-bit immediate argument
static uint8_t im8(z80_state *s)
{
	return s->m_rd(s, s->PC++);
}

//Returns an instruction 16-bit immediate argument
static uint16_t im16(z80_state *s)
{
	uint16_t ret = m_rd16(s, s->PC);
	s->PC += 2;
	return ret;
}

//Load WZ register with IXIY plus offset
static void loadWZ(z80_state *s)
{
	switch (s->ixiy)
	{
	default:
	case 0: break;
	case 1: s->WZ = s->IX + (int8_t)im8(s); break;
	case 2: s->WZ = s->IY + (int8_t)im8(s); break;
	}
}

//Reads from a register or memory selected by the 3-bit argument sel, if ix is true HL->IX/IY
static uint8_t reg8rd(z80_state *s, uint8_t sel, uint8_t ix)
{
	switch (sel & 0x7) //3-bit
	{
	case 0: return s->B;
	case 1: return s->C;
	case 2: return s->D;
	case 3: return s->E;
	case 4:
		switch (ix)
		{
		default:
		case 0: return s->H;
		case 1: return s->IXH;
		case 2: return s->IYH;
		}
		break;
	case 5:
		switch (ix)
		{
		default:
		case 0: return s->L;
		case 1: return s->IXL;
		case 2: return s->IYL;
		}
		break;
	case 6: return s->m_rd(s, (ix) ? s->WZ : s->HL);
	case 7: return s->A;
	}
	return 0; //Should not occur
}

//Writes to a register or memory selected by the 3-bit argument sel, if ix is true HL->IX/IY
static void reg8wr(z80_state *s, uint8_t sel, uint8_t val, uint8_t ix)
{
	switch (sel & 0x7) //3-bit
	{
	case 0: s->B = val; break;
	case 1: s->C = val; break;
	case 2: s->D = val; break;
	case 3: s->E = val; break;
	case 4:
		switch (ix)
		{
		case 0: s->H = val; break;
		case 1: s->IXH = val; break;
		case 2: s->IYH = val; break;
		}
		break;
	case 5:
		switch (ix)
		{
		case 0: s->L = val; break;
		case 1: s->IXL = val; break;
		case 2: s->IYL = val; break;
		}
		break;
	case 6: s->m_wr(s, (ix) ? s->WZ : s->HL, val); break;
	case 7: s->A = val; break;
	}
}

//Reads from a register pair selected by the 2-bit argument sel
static uint16_t reg16rd(z80_state *s, uint8_t sel)
{
	switch (sel & 0x3) //2-bit
	{
	case 0: return s->BC;
	case 1: return s->DE;
	case 2:
		switch (s->ixiy)
		{
		case 0: return s->HL;
		case 1: return s->IX;
		case 2: return s->IY;
		}
		break;
	case 3: return s->SP;
	}
	return 0; //Should not occur
}

//Writes to a register pair selected by the 2-bit argument sel
static void reg16wr(z80_state *s, uint8_t sel, uint16_t val)
{
	switch (sel & 0x3) //2-bit
	{
	case 0: s->BC = val; break;
	case 1: s->DE = val; break;
	case 2:
		switch (s->ixiy)
		{
		case 0: s->HL = val; break;
		case 1: s->IX = val; break;
		case 2: s->IY = val; break;
		}
		break;
	case 3: s->SP = val; break;
	}
}

//Returns if the 3-bit condition given by sel is currently true
static int condition(z80_state *s, uint8_t sel)
{
	switch (sel & 0x7) //3-bit
	{
	case 0: return !GET_FLAG(Z);  //NZ
	case 1: return GET_FLAG(Z);   //Z
	case 2: return !GET_FLAG(C);  //NC
	case 3: return GET_FLAG(C);   //C
	case 4: return !GET_FLAG(PV); //PO
	case 5: return GET_FLAG(PV);  //PE
	case 6: return !GET_FLAG(S);  //P
	case 7: return GET_FLAG(S);   //M
	}
	return 0; //Should not occur
}

//Push a 16-bit value onto the stack
static void stk_push(z80_state *s, uint16_t v)
{
	s->SP -= 2;
	m_wr16(s, s->SP, v);
}

//Pop a 16-bit value from the stack
static uint16_t stk_pop(z80_state *s)
{
	s->SP += 2;
	return m_rd16(s, s->SP - 2);
}

//Calculate parity for 8-bit value 1=even 0=odd
static int parity(uint8_t v)
{
	int sum = 0;
	for (int i = 0; i < 8; i++)
	{
		sum += v & 1;
		v >>= 1;
	}
	return !(sum & 0x01);
}

//Performs one of 8 ALU operations operating on A using the argument val
static void alu(z80_state *s, uint8_t op, uint8_t val)
{
	uint8_t carry = GET_FLAG(C);
	switch (op & 0x7) //3-bit
	{
	case 0: //Add
		SET_FLAG(N, 0);
		SET_FLAG(C, (s->A + val) > 0xFF);
		SET_FLAG(H, ((s->A & 0xF) + (val & 0xF)) >= 0x10);
		SET_FLAG(PV, ((int8_t)s->A + (int8_t)val) > 127 || ((int8_t)s->A + (int8_t)val) < -128);
		s->A += val;
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 1: //Add with carry
		SET_FLAG(N, 0);
		SET_FLAG(C, (s->A + val + carry) > 0xFF);
		SET_FLAG(H, ((s->A & 0xF) + (val & 0xF) + carry) >= 0x10);
		SET_FLAG(PV, ((int8_t)s->A + (int8_t)val + carry) > 127 || ((int8_t)s->A + (int8_t)val + carry) < -128);
		s->A += val + carry;
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 2: //Subtract
		SET_FLAG(N, 1);
		SET_FLAG(C, val > s->A);
		SET_FLAG(H, (val & 0xF) > (s->A & 0xF));
		SET_FLAG(PV, ((int8_t)s->A - (int8_t)val) > 127 || ((int8_t)s->A - (int8_t)val) < -128);
		s->A -= val;
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 3: //Subtract with carry
		SET_FLAG(N, 1);
		SET_FLAG(C, (val + carry) > s->A);
		SET_FLAG(H, ((val & 0xF) + carry) > (s->A & 0xF));
		SET_FLAG(PV, ((int8_t)s->A - (int8_t)val - carry) > 127 || ((int8_t)s->A - (int8_t)val - carry) < -128);
		s->A -= val + carry;
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 4: //Bitwise AND
		SET_FLAG(N, 0);
		SET_FLAG(C, 0);
		SET_FLAG(H, 1);
		s->A &= val;
		SET_FLAG(PV, parity(s->A));
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 5: //Bitwise XOR
		SET_FLAG(N, 0);
		SET_FLAG(C, 0);
		SET_FLAG(H, 0);
		s->A ^= val;
		SET_FLAG(PV, parity(s->A));
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 6: //Bitwise OR
		SET_FLAG(N, 0);
		SET_FLAG(C, 0);
		SET_FLAG(H, 0);
		s->A |= val;
		SET_FLAG(PV, parity(s->A));
		SET_FLAG(Z, s->A == 0);
		SET_FLAG(S, s->A & 0x80);
		SET_FLAG(Y, s->A & 0x20);
		SET_FLAG(X, s->A & 0x08);
		break;
	case 7: //Compare
		SET_FLAG(N, 1);
		SET_FLAG(C, val > s->A);
		SET_FLAG(H, (val & 0xF) > (s->A & 0xF));
		SET_FLAG(PV, ((int8_t)s->A - (int8_t)val) > 127 || ((int8_t)s->A - (int8_t)val) < -128);
		SET_FLAG(Z, (s->A - val) == 0);
		SET_FLAG(S, (s->A - val) & 0x80);
		SET_FLAG(Y, val & 0x20);
		SET_FLAG(X, val & 0x08);
		break;
	}
	s->Q = s->F;
}

//Perform shift/rotate operation on argument v and return result
static uint8_t shiftrot(z80_state *s, uint8_t op, uint8_t v)
{
	uint8_t highoct = (op & 030) >> 3; //Operation type
	uint8_t lowoct = op & 07; //Sub-operation or bit

	switch (highoct) //2-bit
	{
	case 0: //Extended shifts and rotates
	{
		uint8_t carry = GET_FLAG(C);
		if (lowoct & 1) //Shift right
			SET_FLAG(C, v & 0x01);
		else //Shift left
			SET_FLAG(C, v & 0x80);

		switch (lowoct) //3-bit
		{
		case 0: //RLC r8/(HL)
			v = (v << 1) | (v >> 7);
			break;
		case 1: //RRC r8/(HL)
			v = (v >> 1) | (v << 7);
			break;
		case 2: //RL r8/(HL)
			v = (v << 1) | carry;
			break;
		case 3: //RR r8/(HL)
			v = (v >> 1) | (carry << 7);
			break;
		case 4: //SLA r8/(HL)
			v <<= 1;
			break;
		case 5: //SRA r8/(HL)
			v = (v & 0x80) | (v >> 1);
			break;
		case 6: //SLL r8/(HL)
			v = (v << 1) | 0x01;
			break;
		case 7: //SRL r8/(HL)
			v >>= 1;
			break;
		}

		SET_FLAG(N, 0);
		SET_FLAG(H, 0);
		SET_FLAG(PV, parity(v));
		SET_FLAG(Z, v == 0);
		SET_FLAG(S, v & 0x80);
		SET_FLAG(Y, v & 0x20);
		SET_FLAG(X, v & 0x08);
		s->Q = s->F;
		break;
	}
	case 1: //BIT n,r8/(HL)
	{
		SET_FLAG(N, 0);
		SET_FLAG(H, 1);
		SET_FLAG(Z, !(v & (1 << lowoct)));
		SET_FLAG(PV, !(v & (1 << lowoct)));
		SET_FLAG(S, (lowoct == 7) && (v & (1 << lowoct)));
		s->Q = s->F;
		break;
	}
	case 2: //RES n,r8/(HL)
		v &= ~(1 << lowoct);
		s->Q = 0;
		break;
	case 3: //SET n,r8/(HL)
		v |= (1 << lowoct);
		s->Q = 0;
		break;
	}
	return v;
}

/*
// Instruction definitions
*/
static int opED(z80_state *s)
{
	uint8_t op = im8(s);
	uint8_t highoct = (op & 0300) >> 6;
	uint8_t midoct = (op & 070) >> 3;
	uint8_t lowoct = op & 07;
	int ret = cycle_ED[op];
	s->Q = 0;

	switch (highoct)
	{
	case 0: //Invalid (NOP)
		break;
	case 1:
		switch (lowoct)
		{
		case 0: //IN r8,(C) IN (C)
		{
			uint8_t v = s->io_rd(s, s->C);
			if (midoct != 6)
				reg8wr(s, midoct, v, s->ixiy);
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(PV, parity(v));
			SET_FLAG(Z, v == 0);
			SET_FLAG(S, v & 0x80);
			SET_FLAG(Y, v & 0x20);
			SET_FLAG(X, v & 0x08);
			s->WZ = s->BC + 1;
			s->Q = s->F;
			break;
		}
		case 1: //OUT (C),r8 OUT (C),0
			s->io_wr(s, s->C, (midoct != 6) ? reg8rd(s, midoct, s->ixiy) : 0);
			s->WZ = s->BC + 1;
			break;
		case 2: //ADC/SBC HL,r16
		{
			uint16_t rhs = reg16rd(s, midoct >> 1) + GET_FLAG(C);
			s->WZ = s->HL + 1;
			if (midoct & 0x1) //ADC HL,r16
			{
				SET_FLAG(N, 0);
				SET_FLAG(C, s->HL + rhs > 0xFFFF);
				SET_FLAG(H, ((s->HL & 0xFFF) + (rhs & 0xFFF)) >= 0x1000);
				SET_FLAG(PV, ((int16_t)s->HL + (int16_t)rhs) > 32767 || ((int16_t)s->HL + (int16_t)rhs) < -32768);
				s->HL += rhs;
			}
			else //SBC HL,r16
			{
				SET_FLAG(N, 1);
				SET_FLAG(C, rhs > s->HL);
				SET_FLAG(H, (rhs & 0xFFF) > (s->HL & 0xFFF));
				SET_FLAG(PV, ((int16_t)s->HL - (int16_t)rhs) > 32767 || ((int16_t)s->HL - (int16_t)rhs) < -32768);
				s->HL -= rhs;
			}
			SET_FLAG(Z, s->HL == 0);
			SET_FLAG(S, s->HL & 0x8000);
			SET_FLAG(Y, s->HL & 0x2000);
			SET_FLAG(X, s->HL & 0x0800);
			s->Q = s->F;
			break;
		}
		case 3: //LD r16,(im16) LD, (im16),r16
			if (midoct & 0x1) //LD r16,(im16)
			{
				uint16_t addr = im16(s);
				reg16wr(s, midoct >> 1, m_rd16(s, addr));
				s->WZ = addr + 1;
			}
			else //LD (im16),r16
			{
				uint16_t addr = im16(s);
				m_wr16(s, addr, reg16rd(s, midoct >> 1));
				s->WZ = addr + 1;
			}
			break;
		case 4: //NEG
		{
			uint8_t v = s->A;
			s->A = 0;
			alu(s, 2, v);
			break;
		}
		case 5: //RETI/RETN
			s->PC = stk_pop(s);
			s->WZ = s->PC;
			s->IFF1 = s->IFF2;
			break;
		case 6: //IM 0/1/2
			switch (midoct & 0x3)
			{
			case 0: //Fallthrough
			case 1: s->IM = 0; break;
			case 2: s->IM = 1; break;
			case 3: s->IM = 2; break;
			}
			break;
		case 7: //I and R loads RRD RLD
			switch (midoct)
			{
			case 0: //LD I,A
				s->I = s->A;
				break;
			case 1: //LD R,A
				s->R = s->A;
				break;
			case 2: //LD A,I
				SET_FLAG(N, 0);
				SET_FLAG(H, 0);
				SET_FLAG(PV, s->IFF2);
				s->A = s->I;
				SET_FLAG(Z, s->A == 0);
				SET_FLAG(S, s->A & 0x80);
				SET_FLAG(Y, s->A & 0x20);
				SET_FLAG(X, s->A & 0x08);
				s->Q = s->F;
				break;
			case 3: //LD A,R
				SET_FLAG(N, 0);
				SET_FLAG(H, 0);
				SET_FLAG(PV, s->IFF2);
				s->A = s->R;
				SET_FLAG(Z, s->A == 0);
				SET_FLAG(S, s->A & 0x80);
				SET_FLAG(Y, s->A & 0x20);
				SET_FLAG(X, s->A & 0x08);
				s->Q = s->F;
				break;
			case 4: //RRD
			{
				uint8_t v = s->m_rd(s, s->HL);
				uint8_t aL = s->A & 0x0F;
				s->A = (s->A & 0xF0) | (v & 0x0F);
				s->m_wr(s, s->HL, (aL << 4) | ((v & 0xF0) >> 4));
				SET_FLAG(N, 0);
				SET_FLAG(H, 0);
				SET_FLAG(PV, parity(s->A));
				SET_FLAG(Z, s->A == 0);
				SET_FLAG(S, s->A & 0x80);
				SET_FLAG(Y, s->A & 0x20);
				SET_FLAG(X, s->A & 0x08);
				s->WZ = s->HL + 1;
				s->Q = s->F;
				break;
			}
			case 5: //RLD
			{
				uint8_t v = s->m_rd(s, s->HL);
				uint8_t aL = s->A & 0x0F;
				s->A = (s->A & 0xF0) | ((v & 0xF0) >> 4);
				s->m_wr(s, s->HL, aL | ((v & 0x0F) << 4));
				SET_FLAG(N, 0);
				SET_FLAG(H, 0);
				SET_FLAG(PV, parity(s->A));
				SET_FLAG(Z, s->A == 0);
				SET_FLAG(S, s->A & 0x80);
				SET_FLAG(Y, s->A & 0x20);
				SET_FLAG(X, s->A & 0x08);
				s->WZ = s->HL + 1;
				s->Q = s->F;
				break;
			}
			case 6: //NOP
			case 7: //NOP
				break;
			}
			break;
		}
		break;
	case 2: //Block instructions
	{
		if (lowoct > 3 && midoct < 4) //NOP
			break;

		int inc = (op & 0x08) ? -1 : 1;
		int rep = op & 0x10;
		switch (lowoct)
		{
		case 0: //LDI LDD LDIR LDDR
		{
			uint8_t v = s->m_rd(s, s->HL);
			s->m_wr(s, s->DE, v);
			s->HL += inc;
			s->DE += inc;
			s->BC--;
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(PV, s->BC != 0);
			SET_FLAG(Y, (s->A + v) & 0x02);
			SET_FLAG(X, (s->A + v) & 0x08);
			if (rep && s->BC)
			{
				s->PC -= 2;
				s->WZ = s->PC + 1;
				SET_FLAG(Y, s->PC & 0x2000);
				SET_FLAG(X, s->PC & 0x0800);
				ret += 5; //Cycle count
			}
			break;
		}
		case 1: //CPI CPD CPIR CPDR
		{
			uint8_t v = s->m_rd(s, s->HL);
			s->HL += inc;
			s->BC--;
			SET_FLAG(N, 1);
			SET_FLAG(H, (v & 0xF) > (s->A & 0xF));
			SET_FLAG(PV, s->BC != 0);
			SET_FLAG(Z, s->A == v);
			SET_FLAG(S, (s->A - v) & 0x80);
			SET_FLAG(Y, (s->A - v - GET_FLAG(H)) & 0x02);
			SET_FLAG(X, (s->A - v - GET_FLAG(H)) & 0x08);
			s->WZ += inc;
			if (rep && s->BC && !GET_FLAG(Z))
			{
				s->PC -= 2;
				s->WZ = s->PC;
				ret += 5; //Cycle count
			}
			break;
		}
		case 2: //INI IND INIR INDR
		{
			uint8_t v = s->io_rd(s, s->C);
			s->m_wr(s, s->HL, v);
			s->HL += inc;
			s->WZ = s->BC + inc;
			s->B--;
			SET_FLAG(N, v & 0x80);
			SET_FLAG(C, (v + s->C + inc) > 255);
			SET_FLAG(H, (v + s->C + inc) > 255);
			SET_FLAG(PV, parity(((v + s->C + inc) & 7) ^ s->B));
			SET_FLAG(Z, s->B == 0);
			SET_FLAG(S, s->B & 0x80);
			SET_FLAG(Y, s->B & 0x20);
			SET_FLAG(X, s->B & 0x08);
			if (rep && s->B)
			{
				s->PC -= 2;
				SET_FLAG(Y, s->PC & 0x2000);
				SET_FLAG(X, s->PC & 0x0800);
				if (GET_FLAG(C))
				{
					if (v & 0x80)
					{
						SET_FLAG(PV, GET_FLAG(PV) ^ parity((s->B - 1) & 7) ^ 1);
						SET_FLAG(H, (s->B & 0x0F) == 0x00);
					}
					else
					{
						SET_FLAG(PV, GET_FLAG(PV) ^ parity((s->B + 1) & 7) ^ 1);
						SET_FLAG(H, (s->B & 0x0F) == 0x0F);
					}
				}
				else
					SET_FLAG(PV, GET_FLAG(PV) ^ parity(s->B & 7) ^ 1);
				ret += 5; //Cycle count
			}
			break;
		}
		case 3: //OUTI OUTD OTIR OTDR
		{
			uint8_t v = s->m_rd(s, s->HL);
			s->io_wr(s, s->C, v);
			s->HL += inc;
			s->B--;
			s->WZ = s->BC + inc;
			SET_FLAG(N, v & 0x80);
			SET_FLAG(C, (v + s->L) > 255);
			SET_FLAG(H, (v + s->L) > 255);
			SET_FLAG(PV, parity(((v + s->L) & 7) ^ s->B));
			SET_FLAG(Z, s->B == 0);
			SET_FLAG(S, s->B & 0x80);
			SET_FLAG(Y, s->B & 0x20);
			SET_FLAG(X, s->B & 0x08);
			if (rep && s->B)
			{
				s->PC -= 2;
				ret += 5; //Cycle count
			}
			break;
		}
		}
		s->Q = s->F;
		break;
	}
	case 3: //Invalid (NOP)
		break;
	}

	s->ixiy = 0;
	return ret;
}

static int opCB(z80_state *s)
{
	uint8_t op = im8(s);
	uint8_t highoct = (op & 0370) >> 3; //Operation
	uint8_t lowoct = op & 07; //Register

	uint8_t v = reg8rd(s, lowoct, s->ixiy);
	v = shiftrot(s, highoct, v);

	//BIT operations set undocumented flags depending on register
	if (((highoct & 030) >> 3) == 1)
	{
		if (lowoct != 6) //Non-(HL)
		{
			SET_FLAG(Y, v & 0x20);
			SET_FLAG(X, v & 0x08);
		}
		else //(HL)
		{
			SET_FLAG(Y, s->WZ & 0x2000);
			SET_FLAG(X, s->WZ & 0x0800);
		}
		s->Q = s->F;
	}
	else //Non-BIT operations write result back to register
		reg8wr(s, lowoct, v, s->ixiy);

	s->ixiy = 0;
	return cycle_CB[op];
}

static int opXYCB(z80_state *s)
{
	loadWZ(s);
	uint8_t op = im8(s);
	uint8_t highoct = (op & 0370) >> 3; //Operation
	uint8_t lowoct = op & 07; //Register

	uint8_t v = s->m_rd(s, s->WZ);
	v = shiftrot(s, highoct, v);

	//Undocumented side effects
	if (((highoct & 030) >> 3) == 1) //BIT operations set XY
	{
		SET_FLAG(Y, s->WZ & 0x2000);
		SET_FLAG(X, s->WZ & 0x0800);
		s->Q = s->F;
	}
	else //Undocumented non-BIT operations also write result to a register
	{
		s->m_wr(s, s->WZ, v);
		if (lowoct != 6) //Undocumented
			reg8wr(s, lowoct, v, 0); //Ensure write to IXL IXH IYL IYH
	}

	s->ixiy = 0;
	return cycle_XYCB[op];
}

static int op0(z80_state *s, uint8_t op)
{
	int ret = cycle_base[op];
	if (s->ixiy)
		ret += cycle_DDFD[op];
	uint8_t highoct = (op & 070) >> 3;
	uint8_t lowoct = (op & 007);

	switch (lowoct)
	{
	case 0: //Relative jumps and misc
		if (highoct & 0x4) //Conditional relative jumps (JR cc,im8)
		{
			int8_t off = (int8_t)im8(s);
			if (condition(s, highoct & 0x3))
			{
				s->PC += off;
				s->WZ = s->PC;
				ret += 5; //Cycle count
			}
		}
		else //Misc
		{
			switch (highoct & 0x3)
			{
			case 0: //NOP
				break;
			case 1: //EX AF,AF'
			{
				uint16_t temp = s->AF;
				s->AF = s->alt_AF;
				s->alt_AF = temp;
				break;
			}
			case 2: //DJNZ im8
			{
				int8_t off = (int8_t)im8(s);
				s->B--;
				if (s->B != 0)
				{
					s->PC += off;
					s->WZ = s->PC;
					ret += 5; //Cycle count
				}
				break;
			}
			case 3: //JR im8
				s->PC += (int8_t)im8(s);
				s->WZ = s->PC;
				break;
			}
		}
		s->Q = 0;
		break;
	case 1: //16-bit add and immediate load
		if (highoct & 0x1) //ADD HL/IX/IY,r16
		{
			uint16_t lhs = reg16rd(s, 2); //HL,IX,IY
			uint16_t rhs = reg16rd(s, highoct >> 1);
			reg16wr(s, 2, lhs + rhs); //HL,IX,IY
			SET_FLAG(N, 0);
			SET_FLAG(C, (lhs + rhs) > 65535);
			SET_FLAG(H, ((lhs & 0xFFF) + (rhs & 0xFFF)) >= 0x1000);
			SET_FLAG(Y, ((lhs + rhs) >> 8) & 0x20);
			SET_FLAG(X, ((lhs + rhs) >> 8) & 0x08);
			s->WZ = lhs + 1;
			s->Q = s->F;
		}
		else //LD r16,im16
		{
			reg16wr(s, highoct >> 1, im16(s));
			s->Q = 0;
		}
		break;
	case 2: //Indirect loads and stores
		switch (highoct)
		{
		case 0: //LD (BC),A
			s->m_wr(s, s->BC, s->A);
			s->WZ = (s->A << 8) | ((s->BC + 1) & 0xFF);
			break;
		case 1: //LD A,(BC)
			s->A = s->m_rd(s, s->BC);
			s->WZ = s->BC + 1;
			break;
		case 2: //LD (DE),A
			s->m_wr(s, s->DE, s->A);
			s->WZ = (s->A << 8) | ((s->DE + 1) & 0xFF);
			break;
		case 3: //LD A,(DE)
			s->A = s->m_rd(s, s->DE);
			s->WZ = s->BC + 1;
			break;
		case 4: //LD (im16),HL/IX/IY
		{
			uint16_t addr = im16(s);
			m_wr16(s, addr, reg16rd(s, 2)); //HL,IX,IY
			s->WZ = addr + 1;
			break;
		}
		case 5: //LD HL/IX/IY,(im16)
		{
			uint16_t addr = im16(s);
			reg16wr(s, 2, m_rd16(s, addr)); //HL,IX,IY
			s->WZ = addr + 1;
			break;
		}
		case 6: //LD (im16),A
		{
			uint16_t addr = im16(s);
			s->m_wr(s, addr, s->A);
			s->WZ = (s->A << 8) | ((addr + 1) & 0xFF);
			break;
		}
		case 7: //LD A,(im16)
		{
			uint16_t addr = im16(s);
			s->A = s->m_rd(s, addr);
			s->WZ = addr + 1;
			break;
		}
		}
		s->Q = 0;
		break;
	case 3: //16-bit inc/decrement (INC r16 DEC r16)
	{
		uint16_t val = reg16rd(s, highoct >> 1);
		val += (highoct & 0x1) ? -1 : 1;
		reg16wr(s, highoct >> 1, val);
		s->Q = 0;
		break;
	}
	case 4: //8-bit register and indirect increment (INC r8 INC (HL))
	{
		if (highoct == 6) //Setup (IX/IY+d) if needed
			loadWZ(s);
		uint8_t val = reg8rd(s, highoct, s->ixiy);
		SET_FLAG(N, 0);
		SET_FLAG(H, ((val & 0xF) + 1) >= 0x10);
		SET_FLAG(PV, val == 0x7F);
		val++;
		SET_FLAG(Z, val == 0);
		SET_FLAG(S, val & 0x80);
		SET_FLAG(Y, val & 0x20);
		SET_FLAG(X, val & 0x08);
		reg8wr(s, highoct, val, s->ixiy);
		s->Q = s->F;
		break;
	}
	case 5: //8-bit register and indirect decrement (DEC r8 DEC (HL))
	{
		if (highoct == 6) //Setup (IX/IY+d) if needed
			loadWZ(s);
		uint8_t val = reg8rd(s, highoct, s->ixiy);
		SET_FLAG(N, 1);
		SET_FLAG(H, 1 > (val & 0xF));
		SET_FLAG(PV, val == 0x80);
		val--;
		SET_FLAG(Z, val == 0);
		SET_FLAG(S, val & 0x80);
		SET_FLAG(Y, val & 0x20);
		SET_FLAG(X, val & 0x08);
		reg8wr(s, highoct, val, s->ixiy);
		s->Q = s->F;
		break;
	}
	case 6: //8-bit immediate load (LD r8,im8)
		if (highoct == 6) //Setup (IX/IY+d) if needed
			loadWZ(s);
		reg8wr(s, highoct, im8(s), s->ixiy);
		s->Q = 0;
		break;
	case 7: //Rotates and misc arithmetic
		switch (highoct)
		{
		case 0: //RLCA
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(C, s->A & 0x80);
			s->A = (s->A << 1) | (s->A >> 7);
			SET_FLAG(Y, s->A & 0x20);
			SET_FLAG(X, s->A & 0x08);
			break;
		case 1: //RRCA
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(C, s->A & 0x01);
			s->A = (s->A >> 1) | (s->A << 7);
			SET_FLAG(Y, s->A & 0x20);
			SET_FLAG(X, s->A & 0x08);
			break;
		case 2: //RLA
		{
			uint8_t carry = GET_FLAG(C);
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(C, s->A & 0x80);
			s->A = (s->A << 1) | (carry);
			SET_FLAG(Y, s->A & 0x20);
			SET_FLAG(X, s->A & 0x08);
			break;
		}
		case 3: //RRA
		{
			uint8_t carry = GET_FLAG(C);
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(C, s->A & 0x01);
			s->A = (s->A >> 1) | (carry << 7);
			SET_FLAG(Y, s->A & 0x20);
			SET_FLAG(X, s->A & 0x08);
			break;
		}
		case 4: //DAA
		{
			//Correction factor
			int8_t cor = 0;
			if (GET_FLAG(H) || ((s->A & 0xF) > 9))
				cor += 0x06;
			if (GET_FLAG(C) || (s->A > 0x99))
			{
				cor += 0x60;
				SET_FLAG(C, 1);
			}
			//Half carry flag
			if (!GET_FLAG(N))
				SET_FLAG(H, ((s->A & 0xF) > 9));
			else
			{
				if (GET_FLAG(H))
					SET_FLAG(H, ((s->A & 0xF) < 6));
				else
					SET_FLAG(H, 0);
			}
			//Correct A
			s->A += (GET_FLAG(N)) ? -cor : cor;
			//Final flags
			SET_FLAG(PV, parity(s->A));
			SET_FLAG(Z, s->A == 0);
			SET_FLAG(S, s->A & 0x80);
			SET_FLAG(Y, s->A & 0x20);
			SET_FLAG(X, s->A & 0x08);
			break;
		}
		case 5: //CPL
			SET_FLAG(N, 1);
			SET_FLAG(H, 1);
			s->A = ~s->A;
			SET_FLAG(Y, s->A & 0x20);
			SET_FLAG(X, s->A & 0x08);
			break;
		case 6: //SCF
			SET_FLAG(N, 0);
			SET_FLAG(H, 0);
			SET_FLAG(C, 1);
			SET_FLAG(Y, ((s->Q ^ s->F) | s->A) & 0x20);
			SET_FLAG(X, ((s->Q ^ s->F) | s->A) & 0x08);
			break;
		case 7: //CCF
			SET_FLAG(N, 0);
			SET_FLAG(H, GET_FLAG(C));
			SET_FLAG(C, !GET_FLAG(C));
			SET_FLAG(Y, ((s->Q ^ s->F) | s->A) & 0x20);
			SET_FLAG(X, ((s->Q ^ s->F) | s->A) & 0x08);
			break;
		}
		s->Q = s->F;
		break;
	}

	s->ixiy = 0;
	return ret;
}

static int op1(z80_state *s, uint8_t op)
{
	int ret = cycle_base[op];
	if (s->ixiy)
		ret += cycle_DDFD[op];
	uint8_t highoct = (op & 070) >> 3; //Destination register
	uint8_t lowoct = (op & 007); //Source register

	if (highoct == 6 && lowoct == 6) //HALT
		s->halted = 1;
	else //Register and HL/IX/IY indirect loads (LD r8,r8 LD (HL),r8 LD r8,(HL))
	{
		if (highoct == 6 || lowoct == 6) //Setup (IX/IY+d) if needed
			loadWZ(s);

		//Read
		uint8_t v;
		if (s->ixiy && highoct == 6) //Storing to (IX/IY+d)?
			v = reg8rd(s, lowoct, 0); //Ensure read from H/L
		else
			v = reg8rd(s, lowoct, s->ixiy);

		//Write
		if (s->ixiy && lowoct == 6) //Was (IX/IY+d) read from?
			reg8wr(s, highoct, v, 0); //Ensure write to H/L
		else
			reg8wr(s, highoct, v, s->ixiy);
	}

	s->Q = 0;
	s->ixiy = 0;
	return ret;
}

static int op2(z80_state *s, uint8_t op)
{
	int ret = cycle_base[op];
	if (s->ixiy)
		ret += cycle_DDFD[op];

	uint8_t highoct = (op & 070) >> 3; //Operation
	uint8_t lowoct = (op & 007); //Register
	if (lowoct == 6)
		loadWZ(s);
	alu(s, highoct, reg8rd(s, lowoct, s->ixiy));

	s->ixiy = 0;
	return ret;
}

static int op3(z80_state *s, uint8_t op)
{
	int ret = cycle_base[op];
	if (s->ixiy)
		ret += cycle_DDFD[op];
	uint8_t lowoct = (op & 007);
	uint8_t highoct = (op & 070) >> 3;
	s->Q = 0;

	switch (lowoct)
	{
	case 0: //Conditional return (RET cc)
		if (condition(s, highoct))
		{
			s->PC = stk_pop(s);
			s->WZ = s->PC;
			ret += 6; //Cycle count
		}
		break;
	case 1: //Stack pop, unconditional return and misc
		if (highoct & 0x1) //Unconditional return and misc
		{
			switch ((highoct & 0x6) >> 1)
			{
			case 0: //RET
				s->PC = stk_pop(s);
				s->WZ = s->PC;
				break;
			case 1: //EXX
			{
				uint16_t temp;
				temp = s->BC;
				s->BC = s->alt_BC;
				s->alt_BC = temp;
				temp = s->DE;
				s->DE = s->alt_DE;
				s->alt_DE = temp;
				temp = s->HL;
				s->HL = s->alt_HL;
				s->alt_HL = temp;
				break;
			}
			case 2: //JP (HL/IX/IY)
				s->PC = reg16rd(s, 2); //HL,IX,IY
				break;
			case 3: //LD SP,HL/IX/IY
				s->SP = reg16rd(s, 2); //HL,IX,IY
				break;
			}
		}
		else //POP
		{
			uint16_t val = stk_pop(s);
			if ((highoct >> 1) == 0x3) //0b11 refers to AF not SP!
				s->AF = val;
			else
				reg16wr(s, highoct >> 1, val);
		}
		break;
	case 2: //Conditional jump (JP cc,im16)
		s->WZ = im16(s);
		if (condition(s, highoct))
			s->PC = s->WZ;
		break;
	case 3: //Unconditional jump, IO, IRQ and misc
		switch (highoct)
		{
		case 0: //JP im16
			s->PC = im16(s);
			s->WZ = s->PC;
			break;
		case 1: //CB Prefix
			s->R = ((s->R + 1) & 0x7F) | (s->R & 0x80); //Increment R
			if (!s->ixiy)
				return ret + opCB(s);
			else
				return ret + opXYCB(s);
			break;
		case 2: //OUT (im8),A
		{
			uint8_t port = im8(s);
			s->WZ = (s->A << 8) | ((port + 1) & 0xFF);
			s->io_wr(s, port, s->A);
			break;
		}
		case 3: //IN A,(im8)
		{
			uint8_t port = im8(s);
			s->WZ = (s->A << 8) + port + 1;
			s->A = s->io_rd(s, port);
			break;
		}
		case 4: //EX (SP),HL
			s->WZ = m_rd16(s, s->SP);
			m_wr16(s, s->SP, reg16rd(s, 2)); //HL,IX,IY
			reg16wr(s, 2, s->WZ); //HL,IX,IY
			break;
		case 5: //EX DE,HL
		{
			uint16_t temp = s->DE;
			s->DE = s->HL;
			s->HL = temp;
			break;
		}
		case 6: //DI
			s->IFF1 = 0;
			s->IFF2 = 0;
			break;
		case 7: //EI
			s->ei_delay = 1;
			break;
		}
		break;
	case 4: //Conditonal call (CALL cc,im16)
		s->WZ = im16(s);
		if (condition(s, highoct))
		{
			stk_push(s, s->PC);
			s->PC = s->WZ;
			ret += 7;
		}
		break;
	case 5: //Stack push and call
		if (highoct & 0x1) //Call and prefix
		{
			switch (highoct >> 1)
			{
			case 0: //CALL
				s->WZ = im16(s);
				stk_push(s, s->PC);
				s->PC = s->WZ;
				break;
			case 1: //DD Prefix (IX)
				s->ixiy = 1;
				return ret;
			case 2: //ED Prefix
				s->R = ((s->R + 1) & 0x7F) | (s->R & 0x80); //Increment R
				return ret + opED(s);
			case 3: //FD Prefix (IY)
				s->ixiy = 2;
				return ret;
			}
		}
		else //PUSH
		{
			uint16_t val;
			if (((highoct & 0x6) >> 1) == 0x3) //0b11 refers to AF not SP!
				val = s->AF;
			else
				val = reg16rd(s, (highoct & 0x6) >> 1);
			stk_push(s, val);
		}
		break;
	case 6: //Intermediate arithmatic
		alu(s, highoct, im8(s));
		break;
	case 7: //RST
		stk_push(s, s->PC);
		s->PC = (highoct * 8);
		s->WZ = s->PC;
		break;
	}

	s->ixiy = 0;
	return ret;
}

static int op(z80_state *s, uint8_t opcode)
{
	s->R = ((s->R + 1) & 0x7F) | (s->R & 0x80);
	switch ((opcode & 0300) >> 6)
	{
	default:
	case 0: return op0(s, opcode); //Misc
	case 1: return op1(s, opcode); //LD r,r and HALT
	case 2: return op2(s, opcode); //ALU ops
	case 3: return op3(s, opcode); //Misc
	}
}

/*
// Public API
*/
void z80_reset(z80_state *s)
{
	//Registers
	s->AF = 0;
	s->BC = 0;
	s->DE = 0;
	s->HL = 0;
	s->IX = 0;
	s->IY = 0;
	s->PC = 0;
	s->SP = 0xFFFF;
	s->WZ = 0;
	s->I = 0;
	s->R = 0;
	s->Q = 0;
	//Alternate registers
	s->alt_AF = 0;
	s->alt_BC = 0;
	s->alt_DE = 0;
	s->alt_HL = 0;
	//Interrupts
	s->IFF1 = 0;
	s->IFF2 = 0;
	s->IM = 0;
	//State flags
	s->halted = 0;
	s->ixiy = 0;
	s->ei_delay = 0;
	s->int_pending = 0;
	s->int_val = 0;
	s->nmi_pending = 0;
}

int z80_cycle(z80_state *s)
{
	int ret = 0;

	if (s->nmi_pending)
	{
		s->nmi_pending = 0;
		s->halted = 0;
		s->IFF1 = 0;
		stk_push(s, s->PC);
		s->PC = 0x0066;
		s->WZ = s->PC;
		s->R++;
		ret += 11;
	}
	else if (s->int_pending && s->IFF1)
	{
		s->int_pending = 0;
		s->halted = 0;
		s->IFF1 = 0;
		s->IFF2 = 0;
		s->R++;

		switch (s->IM)
		{
		default:
		case 0:
			ret += op(s, s->int_val) + 2;
			break;
		case 1:
			stk_push(s, s->PC);
			s->PC = 0x0038;
			s->WZ = s->PC;
			ret += 13;
			break;
		case 2:
			stk_push(s, s->PC);
			s->PC = m_rd16(s, (s->I << 8) | s->int_val);
			s->WZ = s->PC;
			ret += 19;
			break;
		}
	}

	if (s->halted)
		return ret + 4;

	if (s->ei_delay)
	{
		s->ei_delay = 0;
		s->IFF1 = 1;
		s->IFF2 = 1;
	}

	do
		ret += op(s, s->m_rd(s, s->PC++));
	while (s->ixiy != 0); //Finish prefixed instructions

	return ret;
}

void z80_int(z80_state *s, uint8_t v)
{
	s->int_pending = 1;
	s->int_val = v;
}

void z80_nmi(z80_state *s)
{
	s->nmi_pending = 1;
}

#ifdef Z80_DEBUG
void z80_printreg(z80_state *s)
{
	printf("PC:%04X | ", s->PC);
	printf("AF:%04X | ", s->AF);
	printf("BC:%04X | ", s->BC);
	printf("DE:%04X | ", s->DE);
	printf("HL:%04X | ", s->HL);
	printf("SP:%04X | ", s->SP);
	printf("IX:%04X | ", s->IX);
	printf("IY:%04X | ", s->IY);
	printf("IR:%04X | ", (s->I << 8) | s->R);
	printf("WZ:%04X | ", s->WZ);
	printf("Q:%02X | ", s->Q);
	printf("AF':%04X | ", s->alt_AF);
	printf("BC':%04X | ", s->alt_BC);
	printf("DE':%04X | ", s->alt_DE);
	printf("HL':%04X | ", s->alt_HL);
	printf("IFF1:%u | ", s->IFF1);
	printf("IFF2:%u | ", s->IFF2);
	printf("IM:%u | ", s->IM);
	printf("Status: ");
	printf("%c", GET_FLAG(C) ? 'C' : ' ');
	printf("%c", GET_FLAG(N) ? 'N' : ' ');
	printf("%s", GET_FLAG(PV) ? "PV" : "  ");
	printf("%c", GET_FLAG(H) ? 'H' : ' ');
	printf("%c", GET_FLAG(Z) ? 'Z' : ' ');
	printf("%c", GET_FLAG(S) ? 'S' : ' ');
	printf("\n");
}
#endif
