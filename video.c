#include "video.h"

//Set pixel in output array
static inline void set_pixel(video_state *s, unsigned int x, unsigned int y, uint32_t col)
{
	if (x < 224 && y < 288)
		s->screen[y][x] = col;
}

//Convert a color ROM value to a 32-bit RGB value
static inline uint32_t col_to_rgb(uint8_t v)
{
	uint8_t r =
		(!!(v & (1 << 2)) * 0x97) +
		(!!(v & (1 << 1)) * 0x47) +
		(!!(v & (1 << 0)) * 0x21);
	uint8_t g =
		(!!(v & (1 << 5)) * 0x97) +
		(!!(v & (1 << 4)) * 0x47) +
		(!!(v & (1 << 3)) * 0x21);
	uint8_t b =
		(!!(v & (1 << 7)) * 0xAE) +
		(!!(v & (1 << 6)) * 0x51);

	return (r << 16) | (g << 8) | b;
}

//Retrieves palette index for a 8x4 block of pixels
static inline uint8_t strip_val(video_state *s, uint8_t *ptr, uint8_t x, uint8_t y)
{
	uint8_t b = *(ptr + 7 - x);
	int off = 3 - y;
	int lbit = (b >> off) & 1;
	int hbit = (b >> (off + 4)) & 1;
	return (hbit << 1) | lbit;
}

//Retrieves palette index for given tile and x y coordinate
static uint8_t tile_val(video_state *s, uint8_t id, uint8_t x, uint8_t y)
{
	uint8_t *ptr = &(s->ROM_tile[id * 16]);
	if (y >= 0 && y <= 3) //Top half
		ptr += 8; //Stored in last 8 bytes
	return strip_val(s, ptr, x, y % 4);
}

//Retrieves palette index for given sprite and x y coordinate
static uint8_t sprite_val(video_state *s, uint8_t id, uint8_t x, uint8_t y)
{
	uint8_t *ptr = &(s->ROM_sprite[id * 64]);
	if (x >= 0 && x <= 7) //Left half of sprite
		ptr += 32;
	const uint8_t off[4] = { 8, 16, 24, 0 }; //Choose vertical quadrant
	ptr += off[y / 4];
	return strip_val(s, ptr, x % 8, y % 4);
}

//Draw tile with given id and palette to background
static void draw_tile(video_state *s, uint8_t id, uint8_t pal, int x, int y)
{
	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			uint8_t pal_idx = (pal << 2) | tile_val(s, id, i, j);
			uint8_t col_idx = s->ROM_palette[pal_idx];
			uint8_t col = s->ROM_colour[col_idx];
			set_pixel(s, (x * 8) + i, (y * 8) + j, col_to_rgb(col));
		}
	}
}

//Draw sprite with givern id to foreground
static void draw_spr(video_state *s, uint8_t sprite)
{
	//Unpack sprite infomation
	uint8_t sx = s->sprite_xy[sprite * 2];
	uint8_t sy = s->sprite_xy[sprite * 2 + 1];
	uint8_t attrib = s->sprite_attrib[sprite * 2];
	uint8_t spr = attrib >> 2;
	uint8_t x_flip = (attrib >> 1) & 1;
	uint8_t y_flip = attrib & 1;
	uint8_t pal = s->sprite_attrib[sprite * 2 + 1];

	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			uint8_t pal_idx = (pal << 2) | sprite_val(s, spr, i, j);
			uint8_t col_idx = s->ROM_palette[pal_idx];
			uint8_t col = s->ROM_colour[col_idx];

			//Transparent?
			if (col == 0)
				continue;

			//Flip?
			int x = (!x_flip) ? i : (15 - i);
			int y = (!y_flip) ? j : (15 - j);

			//Convert to screen coords (origin bottom right)
			int scr_x = 224 + 15 - sx;
			int scr_y = 288 - 16 - sy;

			//Draw
			set_pixel(s, scr_x + x, scr_y + y, col_to_rgb(col));
		}
	}
}

//Draw status row from VRAM address addr
static void draw_statusrow(video_state *s, uint8_t row, uint16_t addr)
{
	for (int col = 0; col < 28; col++)
	{
		uint16_t tile_addr = addr + 27 - col; //Stored right to left
		draw_tile(s, s->VRAM_tile[tile_addr], s->VRAM_pal[tile_addr], col, row);
	}
}

//Draw main gameplay tiles
static void draw_board(video_state *s)
{
	for (int col = 0; col < 28; col++)
	{
		for (int row = 2; row < 34; row++)
		{
			uint16_t tile_addr = 0x040 + (0x20 * (27 - col)) + row - 2;
			draw_tile(s, s->VRAM_tile[tile_addr], s->VRAM_pal[tile_addr], col, row);
		}
	}
}

//Draw video to buffer
void video_draw(video_state *s)
{
	//Draw tiles
	draw_statusrow(s, 0, 0x3C2);
	draw_statusrow(s, 1, 0x3E2);
	draw_board(s);
	draw_statusrow(s, 34, 0x002);
	draw_statusrow(s, 35, 0x022);

	//Draw sprites
	for (int i = 7; i >= 0; i--) //7=lowest priority 0=highest priority
		draw_spr(s, i);
}
