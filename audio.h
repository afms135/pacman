#ifndef AUDIO_H
#define AUDIO_H
#include <stdint.h>

typedef struct audio_state
{
	//Waveform ROMs
	uint8_t ROM[512];

	//Sound enable
	uint8_t en;

	//Voice 1
	uint8_t v1_acc[5];
	uint8_t v1_wav;
	uint8_t v1_freq[5];
	uint8_t v1_vol;

	//Voice 2
	uint8_t v2_acc[4];
	uint8_t v2_wav;
	uint8_t v2_freq[4];
	uint8_t v2_vol;

	//Voice 3
	uint8_t v3_acc[4];
	uint8_t v3_wav;
	uint8_t v3_freq[4];
	uint8_t v3_vol;
} audio_state;

int16_t audio_cycle(audio_state *s);

#endif /*AUDIO_H*/
