#include "z80.h"
#include "video.h"
#include "audio.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#ifdef _WIN32
#include <SDL.h>
#else
#include "icon.h"
#include <SDL2/SDL.h>
#endif

/*
 * Defines
 */
#define DIP10 (1) //0=freeplay 1=1credit 2=2credit 3=1credit per 2 coins
#define DIP32 (3) //0=1 1=2 2=3 3=5 lives per game
#define DIP54 (0) //0=10000 1=15000 2=20000 3=none points per life
#define DIP6  (1) //0=Hard 1=Normal difficulty
#define DIP7  (1) //0=Alternate 1=Normal ghost names
#define CPU_HZ (3072000)
#define SND_HZ (96000)
#define VID_HZ (60)
#define CYC_PER_FRAME (CPU_HZ/VID_HZ)
#define CYC_PER_SAMPLE (CPU_HZ/SND_HZ)
#define SAMPLES_PER_FRAME (SND_HZ/VID_HZ)

/*
 * Globals
 */
static z80_state s;
static video_state vs;
static audio_state as;
static uint8_t ROM[16384];
static uint8_t RAM[2032];
static uint8_t irq_en = 0;
static uint8_t irq_vec = 0;
static uint8_t watchdog = 0;
static int init_complete = 0;
static SDL_Window *win = NULL;
static SDL_Renderer *ren = NULL;
static SDL_Texture *tex = NULL;
static SDL_AudioDeviceID aid = 0;
static const Uint8 *keys = NULL;

/*
 * Forward declarations
 */
static uint8_t mem_rd(z80_state *s, uint16_t addr);
static void mem_wr(z80_state *s, uint16_t addr, uint8_t v);
static uint8_t io_rd(z80_state *s, uint8_t port);
static void io_wr(z80_state *s, uint8_t port, uint8_t v);
static char *string_concat(size_t n, ...);
static char *hiscore_path(void);
static int load_ROM(const char *file, void *ptr, size_t len);
static void load_hiscore(void);
static void save_hiscore(void);

/*
 * Main
 */
int main(int argc, char *argv[])
{
	//Init SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		fprintf(stderr, "Error: SDL_Init() %s\n", SDL_GetError());
		goto ERR;
	}

	win = SDL_CreateWindow("Pac-Man", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 224, 288, 0);
	if (win == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL_CreateWindow()", SDL_GetError(), NULL);
		goto ERR;
	}

	ren = SDL_CreateRenderer(win, -1, 0);
	if (ren == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL_CreateRenderer()", SDL_GetError(), win);
		goto ERR;
	}

	if (!SDL_RenderSetLogicalSize(ren, 224, 288) && !SDL_RenderSetIntegerScale(ren, SDL_TRUE))
	{
		SDL_SetWindowMinimumSize(win, 224, 288);
		SDL_SetWindowResizable(win, SDL_TRUE);
	}

	tex = SDL_CreateTexture(ren, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, 224, 288);
	if (tex == NULL)
	{
		SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL_CreateTexture()", SDL_GetError(), win);
		goto ERR;
	}

	keys = SDL_GetKeyboardState(NULL);

	//Init SDL audio
	if (SDL_Init(SDL_INIT_AUDIO) == 0)
	{
		SDL_AudioSpec spec = {0};
		spec.freq = SND_HZ;
		spec.format = AUDIO_S16;
		spec.channels = 1;
		spec.samples = SAMPLES_PER_FRAME;
		spec.callback = NULL;
		aid = SDL_OpenAudioDevice(NULL, 0, &spec, NULL, 0);
	}

	//Set application icon
#ifdef _WIN32
	SDL_SetHint(SDL_HINT_WINDOWS_INTRESOURCE_ICON, "101");
	SDL_SetHint(SDL_HINT_WINDOWS_INTRESOURCE_ICON_SMALL, "101");
#else
	SDL_RWops *ops = SDL_RWFromConstMem(icon_bmp, icon_bmp_len);
	if (ops != NULL)
	{
		SDL_Surface *icon = SDL_LoadBMP_RW(ops, 1);
		if (icon != NULL)
			SDL_SetWindowIcon(win, icon);
		SDL_FreeSurface(icon);
	}
#endif

	//Load ROMS
	if (
		load_ROM("pacman.6e", &ROM[0x0000],   4096) ||
		load_ROM("pacman.6f", &ROM[0x1000],   4096) ||
		load_ROM("pacman.6h", &ROM[0x2000],   4096) ||
		load_ROM("pacman.6j", &ROM[0x3000],   4096) ||
		load_ROM("pacman.5e", vs.ROM_tile,    4096) ||
		load_ROM("pacman.5f", vs.ROM_sprite,  4096) ||
		load_ROM("82s126.4a", vs.ROM_palette,  256) ||
		load_ROM("82s123.7f", vs.ROM_colour,    32) ||
		load_ROM("82s126.1m", &as.ROM[0x000],  256) ||
		load_ROM("82s126.3m", &as.ROM[0x100],  256)
	)
		goto ERR;

	//Reset Z80
	s.m_rd = mem_rd;
	s.m_wr = mem_wr;
	s.io_rd = io_rd;
	s.io_wr = io_wr;
	z80_reset(&s);

	//Start Emulation
	SDL_PauseAudioDevice(aid, 0);
	int running = 1;
	int mute = 0;
	while (running)
	{
		Uint32 framestart = SDL_GetTicks();

		//Clear screen
		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);

		//Check events
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
				running = 0;
			if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_m)
				mute = !mute;
		}

		//Wait until more audio samples are needed (audio sync)
		if (aid)
			while (SDL_GetQueuedAudioSize(aid) > SAMPLES_PER_FRAME)
				SDL_Delay(1);

		//Cycle CPU and audio
		for (int i = 0; i < SAMPLES_PER_FRAME; i++)
		{
			for (int j = 0; j < CYC_PER_SAMPLE;)
				j += z80_cycle(&s);

			int16_t sval = audio_cycle(&as);
			if (mute)
				sval = 0;
			if (aid)
				SDL_QueueAudio(aid, &sval, sizeof(sval));
		}

		//V-Blank Interrupt and watchdog
		if (irq_en)
			z80_int(&s, irq_vec);
		watchdog++;
		if (watchdog == 16)
			z80_reset(&s);

		//Render
		video_draw(&vs);

		//Copy to texture
		void *ptr;
		int pitch;
		SDL_LockTexture(tex, NULL, &ptr, &pitch);
		memcpy(ptr, vs.screen, 224 * 288 * 4);
		SDL_UnlockTexture(tex);

		//Copy to screen
		SDL_RenderCopy(ren, tex, NULL, NULL);
		SDL_RenderPresent(ren);

		//Vsync (video sync)
		if (!aid)
		{
			Uint32 frameend = SDL_GetTicks();
			if (frameend - framestart < (1000 / VID_HZ))
				SDL_Delay((1000 / VID_HZ) - (frameend - framestart));
		}
	}

	//Save high score
	if (init_complete)
		save_hiscore();

	//Cleanup
ERR:
	if (aid > 0)
		SDL_CloseAudioDevice(aid);
	if (tex != NULL)
		SDL_DestroyTexture(tex);
	if (ren != NULL)
		SDL_DestroyRenderer(ren);
	if (win != NULL)
		SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}

/*
 * Z80 Callbacks
 */
static uint8_t mem_rd(z80_state *s, uint16_t addr)
{
#define MAPARR(low, high, mem) if (addr >= (low) && addr <= (high)) { return (mem)[addr - (low)]; }
#define MAPREG(address, reg) if (addr == (address)) { return (reg); }
#define BKPT(address) ((s->PC - 1) == (address) && addr == (address))

	//A15 not connected
	addr &= 0x7FFF;

	//Save hiscore before init code clears memory
	if (BKPT(0x0000))
	{
		if (init_complete)
			save_hiscore();
		init_complete = 0;
	}
	//Load saved hiscore after init tasks finish
	if (BKPT(0x2698)) //End of last task in init
	{
		load_hiscore();
		init_complete = 1;
	}

	//Update keys array if input requested
	if (addr == 0x5000 || addr == 0x5040)
		SDL_PumpEvents();

	//Memory
	MAPARR(0x0000, 0x3FFF, ROM);              //ROM
	MAPARR(0x4000, 0x43FF, vs.VRAM_tile);     //Tile RAM
	MAPARR(0x4400, 0x47FF, vs.VRAM_pal);      //Palette RAM
	MAPARR(0x4800, 0x4FEF, RAM);              //RAM
	MAPARR(0x4FF0, 0x4FFF, vs.sprite_attrib); //Sprite attributes

	//Registers
	MAPREG(0x5000, //Player input (IN0)
		(!keys[SDL_GetScancodeFromKey(SDLK_c)] << 7) | //Credit
		(1 << 6) | //Coin slot 2 (not implemented)
		(1 << 5) | //Coin slot 1 (not implemented)
		(!keys[SDL_GetScancodeFromKey(SDLK_r)] << 4) | //Rack advance
		(!keys[SDL_SCANCODE_DOWN] << 3) |  //P1 Down
		(!keys[SDL_SCANCODE_RIGHT] << 2) | //P1 Right
		(!keys[SDL_SCANCODE_LEFT] << 1) |  //P1 Left
		(!keys[SDL_SCANCODE_UP] << 0)      //P1 Up
	);
	MAPREG(0x5040, //Player input (IN1)
		(1 << 7) | //0=Cocktail 1=Cabinet
		(!keys[SDL_GetScancodeFromKey(SDLK_2)] << 6) | //2-player start
		(!(
			keys[SDL_SCANCODE_RETURN] ||
			keys[SDL_GetScancodeFromKey(SDLK_1)]
		) << 5) | //1-player start
		(!keys[SDL_GetScancodeFromKey(SDLK_t)] << 4) | //Board test / tilt
		(!keys[SDL_SCANCODE_S] << 3) | //P2 Down
		(!keys[SDL_SCANCODE_D] << 2) | //P2 Right
		(!keys[SDL_SCANCODE_A] << 1) | //P2 Left
		(!keys[SDL_SCANCODE_W] << 0)   //P2 Up
	);
	MAPREG(0x5080, //DIP switches
		(DIP7 << 7) |
		(DIP6 << 6) |
		(DIP54 << 4) |
		(DIP32 << 2) |
		(DIP10 << 0)
	);

	fprintf(stderr, "Invalid read from address %04X PC=%04X\n", addr, s->PC);
	return 0x00; //NOP

#undef MAPARR
#undef MAPREG
}

static void mem_wr(z80_state *s, uint16_t addr, uint8_t v)
{
#define MAPARR(low, high, mem, val) if (addr >= (low) && addr <= (high)) { (mem)[addr - (low)] = (val); return; }
#define MAPREG(address, reg, val) if (addr == (address)) { (reg) = (val); return; }
#define MAPNUL(low, high) if (addr >= (low) && addr <= (high)) { return; }

	//A15 not connected
	addr &= 0x7FFF;

	//RAM
	MAPARR(0x4000, 0x43FF, vs.VRAM_tile, v);     //Tile RAM
	MAPARR(0x4400, 0x47FF, vs.VRAM_pal, v);      //Palette RAM
	MAPARR(0x4800, 0x4FEF, RAM, v);              //RAM
	MAPARR(0x4FF0, 0x4FFF, vs.sprite_attrib, v); //Sprite attributes

	//Registers
	MAPREG(0x5000, irq_en, v & 1); //Interrupt enable
	MAPREG(0x5001, as.en, v & 1);  //Audio enable
	MAPNUL(0x5002, 0x5002); //Not connected on hardware, but game clears range 0x5000-0x5007
	MAPNUL(0x5003, 0x5003); //Flip screen (not implemented, for cocktail)
	MAPNUL(0x5004, 0x5004); //Player 1 start lamp (not connected on hardware)
	MAPNUL(0x5005, 0x5005); //Player 2 start lamp (not connected on hardware)
	MAPNUL(0x5006, 0x5006); //Coin lockout (not connected on hardware)
	MAPNUL(0x5007, 0x5007); //Coin counter (not implemented)

	//Audio registers (lower nibble)
	MAPARR(0x5040, 0x5044, as.v1_acc,  v & 0xF); //Voice 1 accumulator
	MAPREG(0x5045,         as.v1_wav,  v & 0xF); //Voice 1 waveform
	MAPARR(0x5046, 0x5049, as.v2_acc,  v & 0xF); //Voice 2 accumulator
	MAPREG(0x504A,         as.v2_wav,  v & 0xF); //Voice 2 waveform
	MAPARR(0x504B, 0x504E, as.v3_acc,  v & 0xF); //Voice 3 accumulator
	MAPREG(0x504F,         as.v3_wav,  v & 0xF); //Voice 3 waveform
	MAPARR(0x5050, 0x5054, as.v1_freq, v & 0xF); //Voice 1 frequency
	MAPREG(0x5055,         as.v1_vol,  v & 0xF); //Voice 1 volume
	MAPARR(0x5056, 0x5059, as.v2_freq, v & 0xF); //Voice 2 frequency
	MAPREG(0x505A,         as.v2_vol,  v & 0xF); //Voice 2 volume
	MAPARR(0x505B, 0x505E, as.v3_freq, v & 0xF); //Voice 3 frequency
	MAPREG(0x505F,         as.v3_vol,  v & 0xF); //Voice 3 volume

	//Other
	MAPARR(0x5060, 0x506F, vs.sprite_xy, v); //Sprite x y position
	MAPNUL(0x5070, 0x507F); //Does not exist, but game clears range 0x5040-0x507F
	MAPREG(0x50C0, watchdog, 0); //Watchdog reset
	MAPNUL(0x7FFD, 0x7FFE); //Does not exist, but game tests interrupts before stack is ready

	fprintf(stderr, "Invalid write %02X to address %04X PC=%04X\n", v, addr, s->PC);

#undef MAPARR
#undef MAPREG
#undef MAPNUL
}

static uint8_t io_rd(z80_state *s, uint8_t port)
{
	fprintf(stderr, "Invalid read from port %02X PC=%04X\n", port, s->PC);
	return 0;
}

static void io_wr(z80_state *s, uint8_t port, uint8_t v)
{
	if (port == 0)
		irq_vec = v;
	else
		fprintf(stderr, "Invalid write %02X to port %02X PC=%04X\n", v, port, s->PC);
}

/*
 * File I/O Helpers
 */
static char *string_concat(size_t n, ...)
{
	size_t len = 1; //NULL terminator
	char *ret = malloc(len);
	if (ret == NULL)
		return NULL;
	strcpy(ret, ""); //Empty

	va_list args;
	va_start(args, n);

	while (n)
	{
		const char *str = va_arg(args, const char*);
		len += strlen(str);

		void *buf = realloc(ret, len);
		if (buf == NULL)
		{
			free(ret);
			va_end(args);
			return NULL;
		}
		ret = buf;

		strcat(ret, str);
		n--;
	}

	va_end(args);
	return ret;
}

static char *hiscore_path(void)
{
	const char *file_path = "score.bin";
	char *folder_path = SDL_GetPrefPath("afms135", "Pac-Man");
	if (folder_path == NULL)
		return NULL;

	char *ret = string_concat(2, folder_path, file_path);
	SDL_free(folder_path);
	return ret;
}

/*
 * File I/O
 */
static int load_ROM(const char *file, void *ptr, size_t len)
{
	char *msg = "";

	FILE *f = fopen(file, "rb");
	if (f == NULL)
	{
		msg = string_concat(4, "Error opening ", file, ": ", strerror(errno));
		goto ERR;
	}

	size_t read = fread(ptr, 1, len, f);
	if (read != len)
	{
		if (ferror(f))
			msg = string_concat(4, "Error reading ", file, ": ", strerror(errno));
		else
			msg = string_concat(3, "Invalid ROM size", ": ", file);
		goto ERR;
	}

	fclose(f);
	return 0;

ERR:
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Pac-Man", msg, win);
	free(msg);
	if (f) fclose(f);
	return -1;
}

static void load_hiscore(void)
{
	FILE *f = NULL;
	char *path = hiscore_path();
	if (path == NULL)
	{
		fprintf(stderr, "Error: Could not locate save path\n");
		goto ERR;
	}

	f = fopen(path, "rb");
	if (f == NULL)
	{
		fprintf(stderr, "Error: Could not open file %s: %s\n", path, strerror(errno));
		goto ERR;
	}

	uint8_t buf[3];
	if (fread(buf, 1, 3, f) != 3)
	{
		fprintf(stderr, "Error: Could not read file %s: %s\n", path, strerror(errno));
		goto ERR;
	}

	//Load score variable
	RAM[0x688] = buf[0];
	RAM[0x689] = buf[1];
	RAM[0x68A] = buf[2];

	//Draw score to screen
	int flag = 0;
	for (int i = 0; i < 6; i++)
	{
		//Unpack BCD
		int n = (i & 1) ? (buf[(5 - i) >> 1] & 0xF) : (buf[(5 - i) >> 1] >> 4);
		if (n || flag)
		{
			flag = 1;
			vs.VRAM_tile[0x3F2 - i] = n; //Tile 0-9
		}
		else
			vs.VRAM_tile[0x3F2 - i] = 0x40; //Blank tile
	}

ERR:
	if (f != NULL) fclose(f);
	if (path != NULL) free(path);
}

static void save_hiscore(void)
{
	FILE *f = NULL;
	char *path = hiscore_path();
	if (path == NULL)
	{
		fprintf(stderr, "Error: Could not locate save path\n");
		goto ERR;
	}

	f = fopen(path, "wb");
	if (f == NULL)
	{
		fprintf(stderr, "Error: Could not open file %s: %s\n", path, strerror(errno));
		goto ERR;
	}

	if (fwrite(&RAM[0x688], 1, 3, f) != 3)
	{
		fprintf(stderr, "Error: Could not write file %s: %s\n", path, strerror(errno));
		goto ERR;
	}

	if (fflush(f) == EOF)
		fprintf(stderr, "Error: Coule not flush data to file %s: %s\n", path, strerror(errno));

ERR:
	if (f != NULL) fclose(f);
	if (path != NULL) free(path);
}
